<?php
$connect = mysqli_connect("localhost", "root", "", "employee");
$query = "SELECT count(*) as present_absent_count, attendance,
     case
         when attendance = 1 then 'Present'
         when attendance = -1 then 'Absent'
       end as attendance FROM employee GROUP BY attendance ;";
$result = mysqli_query($connect, $query);
$i=0;
while ($row = mysqli_fetch_array($result)) {
    $label[$i] = $row["attendance"];
    $count[$i] = $row["present_absent_count"];
    $i++;
}
?>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Attendance Graph</title>
<style>
body {
    width: 660px;
    margin: 0 auto;
}
</style>
<script type="text/javascript"
    src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">  
google.charts.load('current', {'packages':['corechart']});  
google.charts.setOnLoadCallback(drawPieChart);  

function drawPieChart()  
{  
    var pie = google.visualization.arrayToDataTable([  
              ['attendancede', 'Numbder'],
              ['<?php echo $label[0]; ?>', <?php echo $count[0]; ?>],
              ['<?php echo $label[1]; ?>', <?php echo $count[1]; ?>],
                    
         ]);  
    var header = {  
          title: 'Percentage of employee attendance',
          slices: {0: {color: '#666666'}, 1:{color: '#006EFF'}}
         };  
    var piechart = new google.visualization.PieChart(document.getElementById('piechart'));  
    piechart.draw(pie, header);  
} 

google.charts.load('current', {packages: ['corechart', 'bar']});
google.charts.setOnLoadCallback(drawColumnChart);

function drawColumnChart() {
    var bar = google.visualization.arrayToDataTable([
           ['attendance','Count',{ role: "style" } ],
           ['<?php echo $label[1]; ?>', <?php echo $count[1]; ?>,"#006EFF"],
           ['<?php echo $label[0]; ?>', <?php echo $count[0]; ?>,"#666666"]
           ]);
    var columnview = new google.visualization.DataView(bar);
     columnview.setColumns([0, 1,
               { calc: "stringify",
                 sourceColumn: 1,
                 type: "string",
                 role: "annotation" },
               2]);       
    var header = {
    title: 'Employee Attendance',
    bar: {groupWidth: "50%"}
    };
    var barchart = new google.visualization.ColumnChart(document.getElementById("columnchart"));
    barchart.draw(columnview, header);
}
</script>
</head>
<body>
    <h1>Attendance Graph</h1>
    <div id="piechart"></div>
    <div id="columnchart"></div>
    </div>
</body>
</html>
