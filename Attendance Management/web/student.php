<?php require_once "web/header.php"; ?>
    <div class="container" style=" margin-top: 20px;">
        <a id="btnAddAction" class="btn btn-primary" href="index.php?action=student-add"><img src="web/image/icon-add.png" />Add Student</a>
    </div>
    <div class="table-responsive" id="toys-grid">
        <table class="table" cellpadding="10" cellspacing="1">
            <thead>
                <tr>
                    <th><strong>Student Name</strong></th>
                    <th><strong>Roll Number</strong></th>
                    <th><strong>Date of Birth</strong></th>
                    <th><strong>Class</strong></th>
                    <th><strong>Action</strong></th>

                </tr>
            </thead>
            <tbody>
                    <?php
                    if (! empty($result)) {
                        foreach ($result as $k => $v) {
                            ?>
          <tr>
                    <td><?php echo $result[$k]["name"]; ?></td>
                    <td><?php echo $result[$k]["roll_number"]; ?></td>
                    <td><?php echo $result[$k]["dob"]; ?></td>
                    <td><?php echo $result[$k]["class"]; ?></td>
                    <td><a class="btnEditAction"
                        href="index.php?action=student-edit&id=<?php echo $result[$k]["id"]; ?>">
                        <img src="web/image/icon-edit.png" />
                        </a>
                        <a class="btnDeleteAction" 
                        href="index.php?action=student-delete&id=<?php echo $result[$k]["id"]; ?>" onclick="return del();">
                        <img src="web/image/icon-delete.png" />
                        </a>
                    </td>
                </tr>
                    <?php
                        }
                    }
                    ?>
                           
            <tbody>
        </table>
    </div>
    <script type="text/javascript">
        function del(){
            var ans=confirm("Do you want to delete this record?");
            if(ans==true){
                return true;
            }else{
                return false;
            }
            return false;
        }

    </script>
</body>
</html>